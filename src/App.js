import { mapGetters, mapMutations, mapState } from 'vuex';
import { Header } from './components';

export default {
    name: 'app',
    components: {
        Header
    },
    computed: {
        ...mapState(['posts']),
        ...mapGetters(['fetchPosts']),
        ...mapMutations(['savePost'])
    }
};
