import {fetchWithToken} from '../utils/http';

const apiUrl = process.env.VUE_APP_POST_SERVICE_URL || "http://post.cynthia.team/posts";

const fetchPosts = () => {
    return new Promise((resolve, reject) => {
        fetchWithToken(apiUrl, {method: 'GET'})
            .then(res => resolve(res))
            .catch(err => reject(err));
    });
};

const fetchPostById = (id) => {
    return new Promise((resolve, reject) => {
        fetchWithToken(`${apiUrl}/${id}`, {method: 'GET'})
            .then(res => resolve(res))
            .catch(err => reject(err));
    });
};

const savePost = (payload) => {
    return new Promise((resolve, reject) => {
        fetchWithToken(apiUrl, {method: 'GET', body: payload})
            .then(() => resolve(payload))
            .catch(err => reject(err));
    });
};

const deletePostById = (id) => {
    return new Promise((resolve) => {
        resolve(id);
        // fetchWithToken(`${apiUrl}/${id}`, {method: 'DELETE'})
        //     .then(res => {
        //         if (res.status !== 204) reject('An error as occured');
        //         else resolve(id);
        //     })
        //     .catch(err => reject(err));
    });
};

export {
    fetchPosts,
    fetchPostById,
    savePost,
    deletePostById
}
