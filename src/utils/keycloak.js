import Keycloak from 'keycloak-js';

const keycloak = Keycloak({
    url: process.env.VUE_APP_KEYCLOAK_SERVICE_URL || "http://keycloak.cynthia.team/auth",
    realm: process.env.VUE_APP_KEYCLOAK_REALM || "master",
    clientId: process.env.VUE_APP_KEYCLOAK_CLIENT_ID || "front-cynthia"
});

const init = () => {
    return new Promise((resolve, reject) => {
        keycloak.init({
            onLoad: "login-required",
            promiseType: "native"
        }).then((authenticated) => {
            if(authenticated) resolve({token: keycloak.token, tokenParsed: keycloak.tokenParsed} );
            else reject();
        }).catch((err) => {
            reject(err);
        })
    })
};

const logout = () => keycloak.logout();

export {
    init,
    logout
};
