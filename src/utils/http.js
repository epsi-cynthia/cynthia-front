const token = localStorage.getItem("token");

const fetchWithToken = (url, options) => {
    const headers = {
        ...options.headers,
        'Authorization': `Bearer ${token}`
    };
    return new Promise((resolve, reject) => {
        fetch(url, {...options, headers})
            .then(res => {
                if (!res.ok) reject(res);
                return res.json();
            })
            .then(res => resolve(res))
            .catch(err => reject(err));
    });
};

export {
    fetchWithToken
}
