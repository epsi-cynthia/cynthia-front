import Vue from 'vue';
import VueRouter from 'vue-router';
import { Account, Home, Message, NotFound } from './pages';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/', component: Home },
        { path: '/message', component: Message },
        { path: '/account', component: Account },
        { path: '*', component: NotFound }
    ]
});

export {
    router
};
