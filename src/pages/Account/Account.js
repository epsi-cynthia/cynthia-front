import {mapGetters} from 'vuex';
import {Avatar} from '../../components';

const Account = {
    name: 'account',
    components: {
        'Avatar': Avatar
    },
    computed: {
        ...mapGetters(['getName', 'getEmail', 'getRole'])
    },
};

export default Account;
