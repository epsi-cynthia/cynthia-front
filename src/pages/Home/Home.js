import {mapActions, mapGetters} from 'vuex';
import {store} from '../../store';

const Home = {
    name: 'home',
    computed: {
        ...mapGetters(['getPosts'])
    },
    methods: {
        ...mapActions(['fetchPosts', 'fetchPostById', 'savePost', 'deletePostById'])
    },
    created: () => {
        store.dispatch('fetchPosts');
    }
};

export default Home;
