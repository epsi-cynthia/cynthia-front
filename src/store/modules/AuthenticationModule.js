import {init, logout} from '../../utils/keycloak';

const PostModule = {
    state: {
        token: "",
        tokenParsed: "",
        authenticated: false
    },
    getters: {
        getName: (state) => {
            return state.tokenParsed ? state.tokenParsed.name : "";
        },
        getEmail: (state) => {
            return state.tokenParsed ? state.tokenParsed.email : "";
        },
        getRole: (state) => {
            return state.tokenParsed ? state.tokenParsed.resource_access.account.roles || "GUEST" : "GUEST";
        }
    },
    mutations: {
        setToken: (state, token) => {
            localStorage.setItem("token", token);
            state.token = token;
        },
        setTokenParsed: (state, tokenParsed) => {
            state.tokenParsed = tokenParsed;
        },
        setAuthenticated: (state, authenticated) => {
            state.authenticated = authenticated;
        }
    },
    actions: {
        login: ({commit}) => {
            init().then(({token, tokenParsed}) => {
                commit('setToken', token);
                commit('setTokenParsed', tokenParsed);
                commit('setAuthenticated', true);
            });
        },
        logout: ({commit}) => {
            logout().then(() => {
                commit('setAuthenticated', false);
            });
        }
    }
};

export default PostModule;
