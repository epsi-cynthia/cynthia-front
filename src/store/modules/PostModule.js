import {fetchPosts, fetchPostById, savePost, deletePostById} from '@/services/PostService';

const PostModule = {
    state: {
        isLoading: false,
        hasError: false,
        data: []
    },
    getters: {
        getPosts: (state) => {
            return state.data.filter((post) => post.content !== null);
        }
    },
    mutations: {
        addPost: (state, payload) => {
            state.data = {...state.data, payload};
        },
        setPost: (state, payload) => {
            state.data = payload;
        },
        removePost: (state, id) => {
            state.data = state.data.filter(post => post.id !== id);
        },
        startLoading: (state) => {
            state.isLoading = true;
            state.hasError = false;
            state.data = [];
        },
        stopLoading: (state) => {
            state.isLoading = false;
        },
        setError: (state, error) => {
            state.hasError = error;
        }
    },
    actions: {
        fetchPosts: ({commit}) => {
            commit('startLoading');
            fetchPosts()
                .then(payload => {
                    commit('setPost', payload);
                    commit('stopLoading');
                })
                .catch(error => {
                    commit('setError', error);
                    commit('stopLoading');
                })
        },
        fetchPostById: ({commit}, id) => {
            commit('startLoading');
            fetchPostById(id)
                .then(payload => {
                    commit('setPost', payload);
                    commit('stopLoading');
                })
                .catch(error => {
                    commit('setError', error);
                    commit('stopLoading');
                })
        },
        savePost: ({commit}, payload) => {
            commit('startLoading');
            savePost(payload)
                .then(payload => {
                    commit('addPost', payload);
                    commit('stopLoading');
                })
                .catch(error => {
                    commit('setError', error);
                    commit('stopLoading');
                })
        },
        deletePostById: ({commit}, id) => {
            commit('startLoading');
            deletePostById(id)
                .then(() => {
                    commit('removePost', id);
                    commit('stopLoading');
                })
                .catch(error => {
                    commit('setError', error);
                    commit('stopLoading');
                })
        }
    }
};

export default PostModule;
