import Vue from 'vue';
import Vuex from 'vuex';
import { PostModule, AuthenticationModule } from "./modules";

Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        posts: PostModule,
        authentication: AuthenticationModule,
    }
});
