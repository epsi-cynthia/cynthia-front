FROM node:12.10.0
RUN npm install -g http-server
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 10815
CMD [ "http-server", "dist" ]
